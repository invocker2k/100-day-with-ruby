# Why Methods?

- A method is a reusable section of code written to perform a specific task in a program. 
- You might be wondering why you need to separate your code into methods, rather than just writing everything out in one big chunk. 
- It turns out there are a few good reasons to divide your programs into methods:


# Một số note lưu lý khi đặt tên hàm trong ruby
- It’s a Ruby best practice to end method names that produce boolean values with a question mark.
  - Cụ thể thì các phương thức trả về kiểu boolean thì nên để dấu hỏi chấm cuối tên hàm!

- Một hàm không tên được khai báo như sau: {}

```
  ví dụ: times {puts "Hi"}
```

### Toán tử so sánh 

- sử dụng:
  - a <=> b

- Nó sẽ trả về -1 nếu tham số khối đầu tiên phải đến trước thứ hai
- 1 nếu ngược lại 
- 0 nếu chúng có trọng số bằng nhau, nghĩa là một cái không đứng trước cái kia (tức là nếu hai giá trị bằng nhau)