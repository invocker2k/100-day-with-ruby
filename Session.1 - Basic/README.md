# Data Types: Numbers, Strings, Booleans

- In Ruby, your information (or data) can come in different types. There are three data types in Ruby that we’re interested in right now: Numeric (any number), Boolean (which can be true or false), and String (words or phrases like "I'm learning Ruby!").

# Variables

- One of the most basic concepts in computer programming is the variable. You can think of a variable as a word or name that grasps a single value. For example, let’s say you needed the number 25 from our last example, but you’re not going to use it right away. You can set a variable, say my_num, to grasp the value 25 and hang onto it for later use, like this:

```ruby
my_num = 25
```

- Declaring variables in Ruby is easy: you just write out a name like my_num, use = to assign it a value, and you’re done! If you need to change a variable, no sweat: just type it again and hit = to assign it a new value.

# Math

- Ruby isn’t limited to simple expressions of assignment like my_num = 100
  
- It can also do all the math you learned about in school.

- There are six arithmetic operators we’re going to focus on:
  - Addition (+)
  - Subtraction (-)
  - Multiplication (*)
  - Division (/)
  - Exponentiation (**)
  - Modulo (%)
    
#### The only ones that probably look weird to you are exponentiation and modulo.
  
- Exponentiation raises one number (the base) to the power of the other (the exponent). For example, 2**3 is 8, since 2**3 means “give me 2 * 2 * 2“ (2 multiplied together 3 times). 3**2 is 9 (3 * 3), and so on.
  - Ở ruby thì phép lũy thừa được biểu diễn là 2 dấu sao "**"
    
- Modulo returns the remainder of division. For example, 25 % 7 would be 4, since 7 goes into 25 three times with 4 left over.

# Everything in Ruby is an Object

- Because everything in Ruby is an object

# The '.length' Method

- Methods are summoned using a .. If you have a string, "I love espresso", and take the .length of it, Ruby will return the length of the string (that is, the number of characters—letters, numbers, spaces, and symbols).

``` ruby
"I love espresso".length
# ==> 15 
```