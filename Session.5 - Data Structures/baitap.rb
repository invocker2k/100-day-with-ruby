# puts "Text please: "
textInput = "the rain in Spain falls mainly on the plain"
texts = textInput.split(" ")

textHash =  Hash.new(0)

texts.each{ |text| textHash[text] +=  1 }

textHash.each{ |key, value|
     puts key + " " + value.to_s

}

# Enter your code here.

textHash[543121] = 100

textHash.keep_if {|key, val|  key.is_a?Integer}

textHash.delete_if {|key, value| key % 2 == 0}