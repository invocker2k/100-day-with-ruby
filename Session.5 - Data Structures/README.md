# Data Structures

## Creating Arrays

- Earlier we saw that an array can be used to store a list of values in a single variable. 
- You can stuff any number of numbers in there, you can repeat numbers, and they don’t have to be in numeric order!

```ruby
array = [5, 7, 9, 2, 0] 
```

## Access by Index

- Here’s something interesting about arrays: each element in the array has what’s called an index. 
- The first element is at index 0, the next is at index 1, the following is at index 2, and so on. 
- We can access elements of the array directly through these numbers using brackets, like so:

```rb
array = [5, 7, 9, 2, 0]
array[2]
# returns "9", since "9"
# is at index 2
```

- The diagram below shows how these indices work for our sample array, [5, 7, 9, 2, 0]. 
  - The first element has index 0, the next has 1, the next has 2, and so on.

```rb
        +---+---+---+---+---+
array   | 5 | 7 | 9 | 2 | 0 |
        +---+---+---+---+---+
index     0   1   2   3   4
```
## Hashes trong ruby

- Nếu như bạn muốn một thứ như mảng nhưng các chỉ số không theo thứ tự từ 0 đến cuối mảng.
- chúng ta không muốn sử dụng các con số làm chỉ số.
- Chúng ta cần một cấu trúc mảng mới gọi là hàm băm.

- hàm băm là tập hợp các khóa giá trị (key-value).

```rb
hash = {
  key1 => value1,
  key2 => value2,
  key3 => value3
}
```

- Values are assigned to keys using =>. You can use any Ruby object for a key or value.

## Adding to a Hash

- We can add to a hash two ways: if we created it using literal notation, we can simply add a new key-value pair directly between the curly braces. 
- If we used Hash.new, we can add to the hash using bracket notation:

### Accessing Hash Values

- You can access values in a hash just like an array.
```rb
pets = {
  "Stevie" => "cat",
  "Bowser" => "hamster",
  "Kevin Sorbo" => "fish"
}
 
puts pets["Stevie"]
# will print "cat"
```

### Iterating Over Hashes
- Vòng lặp trong Hash
  - cần 2 biến key và value

vd:
```rb
restaurant_menu.each do |item, price|
  puts "#{item}: #{price}"
end
```

# baitap

- Những gì bạn sẽ xây dựng

- Trong dự án này, chúng tôi sẽ viết một chương trình nhận đầu vào của người dùng, sau đó xây dựng một hàm băm từ đầu vào đó. 
- Mỗi khóa trong hàm băm sẽ là một từ của người dùng; mỗi giá trị sẽ là số lần từ đó xuất hiện. Ví dụ: nếu chương trình của chúng tôi nhận được chuỗi "the rain in Spain falls mainly on the plain,", nó sẽ trả về
```
the 2
falls 1
on 1
mainly 1
in 1
rain 1
plain 1
Spain 1
```