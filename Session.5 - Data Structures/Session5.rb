# Adding to a Hash
pets = Hash.new
pets["Stevie"] = "cat"
# Adds the key "Stevie" with the
# value "cat" to the hash

friends = ["Milhouse", "Ralph", "Nelson", "Otto"]

family = { "Homer" => "dad",
  "Marge" => "mom",
  "Lisa" => "sister",
  "Maggie" => "sister",
  "Abe" => "grandpa",
  "Santa's Little Helper" => "dog"
}

friends.each { |x| puts "#{x}" }
family.each { |x, y| puts "#{x}: #{y}" }

# lặp trong hash

restaurant_menu = {
    "noodles" => 4,
    "soup" => 3,
    "salad" => 2
  }
   
restaurant_menu.each do |item, price|
    puts "#{item}: #{price}"
end