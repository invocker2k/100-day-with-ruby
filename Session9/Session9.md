# Implicit Return
We know that methods in Ruby can return values, and we ask a method to return a value when we want to use it in another part of our program. What if we don’t put a return statement in our method definition, though?

For instance, if you don’t tell a JavaScript function exactly what to return, it’ll return undefined. For Python, the default return value is None. But for Ruby, it’s something different: Ruby’s methods will return the result of the last evaluated expression.
  - VỚi ruby, trả về biểu thức cuối cùng.
  
This means that if you have a Ruby method like this one:

```rb
def add(a,b)
  return a + b
end
# You can simply write:

def add(a,b)
  a + b
end
```
And either way, when you call add(1,1), you’ll get 2.

- Ngắn gọn lại thì, Trong ruby viết tắt của hàm trả về  là câu lệnh cuối cùng.
- Không cần viết return cho câu lenhj cuối. Mác định rb sẽ trả về statement cuối cùng nếu ko có return trước đó.

# Call and Response
Remember when we mentioned that symbols are awesome for referencing method names? Well, .respond_to? takes a symbol and returns true if an object can receive that method and false otherwise. For example,

```rb
[1, 2, 3].respond_to?(:push)
```
would return true, since you can call .push on an array object. However,    

```rb
[1, 2, 3].respond_to?(:to_sym)
```
would return false, since you can’t turn an array into a symbol.

# Being Pushy

- Speaking of pushing to arrays, Ruby has some nice shortcuts for common method names. As luck would have it, one is for .push!

- Instead of typing out the .push method name, you can simply use <<, the concatenation operator (also known as “the shovel”) to add an element to the end of an array:

```rb 
[1, 2, 3] << 4
# ==> [1, 2, 3, 4]
```
- Good news: it also works on strings! You can do:

```rb

"Yukihiro " << "Matsumoto"
# ==> "Yukihiro Matsumoto"
```

# #{} trong rb nhu ${} trong js
- All you need to do is place the variable name inside #{} within a string!