# Hashes and Symbols

### Nil: a Formal Introduction

- Nếu trỏ tới một khóa không tồn tại?
  - Bạn sẽ nhận được một giá tri nil.
  - nil là một giá trị không true trong ruby :)
  - Tuy nhiên false có nghĩa là không đúng, trong đó, nil có nghĩa là không có gì cả.

  ## Rubyist’s 
- Key is string, but not use ""

```rb
    menagerie = { :foxes => 2,
    :giraffe => 1,
    :weezards => 17,
    :elves => 1,
    :canaries => 4,
    :ham => 1
  }

puts menagerie
```

## Symbol Syntax

- Symbols always start with a colon (:)
- They must be valid Ruby variable names, so the first character after the colon has to be a letter or underscore (_); after that, any combination of letters, numbers, and underscores is allowed.

#### What are Symbols Used For?

- Symbols pop up in a lot of places in Ruby, but they’re primarily used either as hash keys or for referencing method names. (We’ll see how symbols can reference methods in a later lesson.)

### Chuyển đổi giữa symbols và string

```rb
:abc.to_s

"abc".to_sym
```
### symbol nhanh hon string( xem trong code "Dare to Compare")

### Cách tìm phần tử  trong hash

- sử dụng hàm select 
```rb
grades = { alice: 100,
  bob: 92,
  chris: 95,
  dave: 97
}
 
grades.select { |name, grade| grade <  97 }
# ==> { :bob => 92, :chris => 95 }
 
grades.select { |k, v| k == :alice }
# ==> { :alice => 100 }
```

### Các hàm tiện dụn khác:

- .each_key  { |k| print k, " " }

- .each_value { |v| print v, " " }
