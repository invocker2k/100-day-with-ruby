breakfast = {
    "bacon" => "tasy",
    "egg"  => "tasty",
    "oatmel" => "Helthy",
    "oj" => "juicy"
} 

# lap cac key value bang vong lap foreach trong ruby

matz = { "First name" => "Yukihiro",
    "Last name" => "Matsumoto",
    "Age" => 47,
    "Nationality" => "Japanese",
    "Nickname" => "Matz"
  }
  
matz.each do |key, value|
puts value
end

#Symbolii

my_first_symbol = :symbolii

# Dare to Compare
require 'benchmark'

string_AZ = Hash[("a".."z").to_a.zip((1..26).to_a)]
symbol_AZ = Hash[(:a..:z).to_a.zip((1..26).to_a)]

string_time = Benchmark.realtime do
  100_000.times { string_AZ["r"] }
end

symbol_time = Benchmark.realtime do
  100_000.times { symbol_AZ[:r] }
end

puts "String time: #{string_time} seconds."
puts "Symbol time: #{symbol_time} seconds."

## thuc hanh sybol

