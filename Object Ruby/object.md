```rb

class Person 
  def initialize(name) 
    @name = name
  end
end

matz = Person.new ("Yukihiro")

```

# scope trong ruby 

- Đầu tiên là biến dùng trong class thì khởi tạo bằng 2 kí tự '@' like so: `@@files.`

- Biến cục bộ. Tức là muốn dùng nó ngoài class. THì khai báo bằng `$,`

- Đẻ kế thừa 1 class ta dùng: `classA < classB` 

- Muốn sử dụng tới hàm của class cha trong phương thức override thì sử dụng từ khóa `super`. `super` sẽ trả về kết quả sau thực thi của method base.

```rb
class Creature
  def initialize(name,age)
    @name = name
    @age = age
  end
  
  def fight
    return "Punch to the chops!"
  end
end

# Add your code below!

class Dragon < Creature
  def initialize(name,age)
    super
  end
    def fight
      puts "Instead of breathing fire..." + super
      super
  end
  end

  a = Dragon.new("a")
  a.fight
```
# multiple inheritance. 
- Đúng rồi đấy, ruby cho phép đa kế thừa.
```rb 
class Creature
  def initialize(name)
    @name = name
  end
end

class Person
  def initialize(name)
    @name = name
  end
end

class Dragon < Creature; end
class Dragon < Person; end
```

# method class( Chỉ dùng trong class)

```rb

class Computer
    @@users
  def Computer.get_users
    @@users
  end
end

```

# method
- Mạc định ruby sẽ để pubic method nếu không khai báo private hay public.

eg public- private
```rb
class Dog
  public
  def bark
    puts "Woof!"
  end
  
  def initialize(name, breed)
    @name = name
    @breed = breed
  end
  private
    def id
      @id_number = 12345
    end
end
```

-  thuộc tính chỉ đọc : `attr_reader`
- thuộc tính chỉ ghi: `attr_writer`
- Cả đọc ghi: `attr_accessor`

# Bạn có “mixins” thay cho các interface.