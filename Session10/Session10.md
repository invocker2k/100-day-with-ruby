# Khối 
Một khối trong ruby sẽ bắt đầu bằng Cặp từ do- end hoặc cặp dấu ngoặc nhọn.

- Yeld là từ khóa cho phép một hàm nhận vào một khối khác và thực thi trong hàm.

```rb
def block_test
  puts "We're in the method!"
  puts "Yielding to the block..."
  yield
  
  puts "We're back in the method!"
end

block_test { puts false ?  ">>> We're in the block!": "no" }
```

Ví dụ trên cho thấy, khối block_test nhận vào một khối bằng từ khóa yield bên trong hàm.

- Ngoài ra, yield cũng có thể nhận vào tham số  viết như sau:

```rb
def double x
  yield x
end

double(2){ |m| puts "#{m*2}" }
```

# Procs

- Tại sao lại dùng proc mà không là khối?
  - VÌ proc là một đối tượng, vì thế chúng có khả năng của đối tượng mà 1 khối không có.
  - Không như khối, proc có thể được sử dụng lại, 

Well, you can also convert symbols to procs using that handy little &.

- Bạn có thể chuyển từ symbols tới procs sử dụng '&'
ví dụ: 

- chúng ta có symbols: `:to_i` chuyển chúng thành proc: `&:to_i`

```rb
strings = ["1", "2", "3"]
nums = strings.map(&:to_i)
# ==> [1, 2, 3]
```


# Lambda

- Giống như proc thì lamda là một đối tượng. Nhưng không dựngf lại ở đó, có một số điểm khác:

Lambdas are defined using the following syntax:

```rb
lambda { |param| block }

# call:
x(&lambda)
```

- Có 2 điểm khác biệt chính giữa proc và lambda:
  - 1: Lambda sẽ thông báo lõi nếu tryền sai số lượng tham số vào lambda. Còn proc sẽ trả về  `nil` với các tham số bị thiếu.
  - Thứ 2, khi trả về một lambda, nó sẽ điều khiển quay trở lại hàm và thực thi các câu lệnh tiếp theo của hàm. Ngược lại, Khi một proc được trả về, nó sẽ thực thi ngay(return hàm về proc) mà không gọi lại hàm. 