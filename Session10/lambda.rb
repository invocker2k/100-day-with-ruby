# lambda không  tham số

def lambda_demo(a_lambda)
    puts "I'm the method!"
    a_lambda.call
end
   
   lambda_demo(-> { 
       puts "I'm the lambda!" 
   })

# lambd có tham số
strings = ["leonardo", "donatello", "raphael", "michaelangelo"]
symbolize = lambda { 
    |par| return  par.to_sym
}


symbols = strings.collect(&symbolize)
print symbols