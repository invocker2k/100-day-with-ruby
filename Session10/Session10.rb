# Block không tham số
def block_test
    puts "We're in the method!"
    puts "Yielding to the block..."
    yield
    
    puts "We're back in the method!"
end
  
block_test { puts ">>> We're in the block!" }

# block có tham số
def double x
    yield x
end
  
double(2){ |m| puts "#{m*2}" }
