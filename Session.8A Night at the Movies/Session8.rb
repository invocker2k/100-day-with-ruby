movies = {
  StarWars: 4.8, 
  Divergent: 4.7
  }

puts "What would you like to do? "
puts "-- Type 'add' to add a movie."
puts "-- Type 'update' to update a movie."
puts "-- Type 'display' to display all movies."
puts "-- Type 'delete' to delete a movie."

choice = gets.chomp

case choice
  when "add"
    title = gets.chomp.to_sym
    rating = gets.chomp.to_i
    if movies[title.to_sym] == nil
    movies[title] = rating
    end
  when "update"
    title = gets.chomp.to_sym
    if movies[title] == nil
    puts "errr"
    else
    rating = gets
    movies[title] = rating.to_i
    end

  when "display"
    movies.each {
      |movie, rating|
      puts "#{movie}: #{rating}"
    }
  when "delete"
    title = gets.chomp.to_sym
    movies.delete(title)
  else
    puts "Error!"
end