- Khởi tạo module:

```rb
module nameM

end

```

- Một số thư viện tích hợp sẵn như math thì không cần gọi, chỉ cần dùng:
```rb
puts Math::Pi
```
- Còn lại thì phải khai báo:

```rb

require 'date'
puts Date.today
```

- ngoài ra có thể include module trong class:

```rb

# Create your module here!
module MartialArts
  def swordsman
    puts "I'm a swordsman."
  end
end




class Ninja

# include here
  include MartialArts
  def initialize(clan)
    @clan = clan
  end
end

class Samurai
  include MartialArts
  def initialize(shogun)
    @shogun = shogun
  end
end
```