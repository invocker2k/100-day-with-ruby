#Danger: Infinite Loops!
def  infiniteLoops
    i = 0
while i < 5
  puts i
  # Add your code here!
  i = i+1
end
end
# infiniteLoops

#The 'Until' Loop
def untilLoop
    i = 0
    until i == 6
    i = i + 1
    end
    puts i
end
# untilLoop

# use {}

def breakLoopUseEnd
    i = 0
    loop {
    i += 1
    print "#{i}"
    break if i > 5
    }

end
# breakLoopUseEnd
def breakLoop
    i = 0
    loop do
    i += 1
    print "#{i}"
    break if i > 5
    end

end
# breakLoop

# each on array
def each_on_array
    array = [1,2,3,4,5]

    array.each { |x|
    x += 10
    print "#{x}"
    }
end

# loop with while

def whileLoop
    count = 1
    while count <= 50
        print count
        count += 1
    end
end

# loop with until
def untilLoop
    j = 3
    until j == 0 do
        print j
        j -= 1
    end
end