# Loops & Iterators

### The 'While' Loop

- Sometimes you want to repeat an action in Ruby while a certain condition is true, but you don’t know how many times you’ll have to repeat that action.
- A good example would be prompting a user for a certain type of input: if they insist on giving you the wrong thing, you may have to re-ask them several times before you get the kind of input you’re looking for.

- To accomplish this, we use something called a while loop. It checks to see if a certain condition is true, and while it is, the loop keeps running.
- As soon as the condition stops being true, the loop stops!

### Loops & Iterators
(Vòng lặp và trình lặp lại)

#### Danger: Infinite Loops!

- Did you see that? The loop printed out the numbers 1 to 10, then stopped. 
- This was because the loop’s condition said to continue while counter was less than 11; since counter went up by 1 each time through the loop, the loop stopped when counter hit 11.

### The 'For' Loop

- Sometimes you do know how many times you’ll be looping, however, and when that’s the case, you’ll want to use a for loop.

##### Inclusive and Exclusive Ranges
- You saw a bit of new syntax in the previous exercise: for num in 1...10. 
- What this says to Ruby is: “For the variable num in the range 1 to 10, do the following.” The following was to puts "#{num}", so as num took on the values of 1 to 9, one at a time, those values were printed to the console.

- The reason this program counted to 9 and not 10 was that we used three dots in the range; this tells Ruby to exclude the final number in the count: for num in 1...10 means “go up to but don’t include 10.” 
- If we use two dots, this tells Ruby to include the highest number in the range.
```ruby
for num in 1..15 # bằng 15
for num in 1...15 # nhỏ hơn 15
```

### The Loop Method

- The simplest iterator is the loop method. 
- You can create a basic (but infinite!) loop by simply typing

```ruby
loop { print "Hello, world!" }
```

- In Ruby, curly braces ({}) are generally interchangeable with the keywords do (to open the block) and end (to close it). 
  - Có thể sử dụng ngoặc nhọn thay thế từ khóa do - end.
- Knowing this, we can write a smarter loop than the one above:

```ruby
i = 0
loop do
  i += 1
  print "#{i}"
  break if i > 5
end
```

### Next!

- The next keyword can be used to skip over certain steps in the loop. 
- For instance, if we don’t want to print out the even numbers, we can write:
  - với next thì vẫn tiếp tục thực thi vòng lặp, ddieuf kiện next true thì sẽ bỏ qua câu lệnh dưới.
```ruby
for i in 1..5
  next if i % 2 == 0
  print i
end
```

### Saving Multiple Values
 - nói là mảng cho nhanh.
 - Let’s say we want to save a range of numbers in a variable. How would we do this? A variable can only hold a single value, right?
 - In Ruby, we can pack multiple values into a single variable using an array. 
 - An array is just a list of items between square brackets, like so: [1, 2, 3, 4]. The items don’t have to be in order—you can just as easily have [10, 31, 19, 400].

 ### The .each Iterator

 - The loop iterator is the simplest, but also one of the least powerful. 
 - A more useful iterator is the .each method, which can apply an expression to each element of an object, one at a time. The syntax looks like this:

```ruby
object.each { |item| 
  # Do something 
}
```

- The variable name between | | can be anything you like: it’s just a placeholder for each element of the object you’re using .each on.
  - giữa || là element trong mảng <3

### Chúng ta có một tiện ích loop nho nhỏ <3

- Sử dụng .time giống như một vòng lặp với đối tượng number:

- For example, if we wanted to print out "Chunky bacon!" ten times, we might type

```rb
10.times { print "Chunky bacon!" }
```

### Looping with 'While'

```rb
i = 3
while i > 0 do
  print i
  i -= 1
end
```

- In the above example, we create a variable called i and set it to 3.
- Then, we print out 321 since we execute the loop as long as i is positive.

# Looping with 'Until'

- Good work!

```rb
i = 3
while i > 0 do
  print i
  i -= 1
end
```
 
```rb
j = 3
until j == 0 do
  print j
  j -= 1
end
```

- In the example above, we wrote the same loop using while and using until.

- Túm cái váy lại thì while dừng vòng lặp nếu dk sai. Ngược lại until thì sẽ dừng vòng lặp nếu điều kiện đúng và còn lặp tới khi đk đúng thì thôi <3


