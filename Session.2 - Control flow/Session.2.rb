print "This is my question?"
answer = gets.chomp
answer2 = answer.capitalize 
answer.capitalize!

### If Else ###

if 1 > 2
    print "I won't get printed because one is less than two."
else
    print "That means I'll get printed!"
end

### if elsif else ###

if x < y  # Assumes x and y are defined
    puts "x is less than y!"
elsif x > y
    puts "x is greater than y!"
else
    puts "x equals y!"
end

### Unless ###

unless hungry
    # Write some sweet programs
else
    # Have some noms
end

### is True? ###

is_true = 2 != 3

is_false = 2 == 3

### Less Than or Greater Than ###

test_1 = 17 > 16

test_2 = 21 < 30

test_3 = 9 >= 9

test_4 = -11 < 4

### Combining Boolean Operators

# boolean_1 = (3 < 4 || false) && (false || true)
boolean_1 = true

# boolean_2 = !true && (!true || 100 != 5**2)
boolean_2 = false


# boolean_3 = true || !(true || false)
boolean_3 = true