# Modifies the value contained within the variable answer itself

- The next line might look a little strange, we don’t assign the result of capitalize to a variable. 
- Instead you might notice the ! at the end of capitalize. 
- This modifies the value contained within the variable answer itself. 
- The next time you use the variable answer you will get the results of answer.capitalize

# Control Flow in Ruby

### If

 - Ruby’s if statement takes an expression, which is just a fancy word for something that has a value that evaluates to either true or false. 
 
- If that expression is true, Ruby executes the block of code that follows the if. 
- If it’s not true (that is, false), Ruby doesn’t execute that block of code: it skips it and goes on to the next thing.

 - Here’s an example of an if statement in action:

 ```ruby
 if 1 < 2
  print "I'm getting printed because one is less than two!"
end
 ```

 - When you’re done with your if, you have to tell Ruby by typing end.

 ### If Else

 ``` ruby
 if 1 > 2
  print "I won't get printed because one is less than two."
else
  print "That means I'll get printed!"
end
 ```

 ### Elsif

- What if you want more than two options, though? 
- It’s elsif to the rescue! 
- The elsif statement can add any number of alternatives to an if/else statement, like so:

```ruby
if x < y  # Assumes x and y are defined
  puts "x is less than y!"
elsif x > y
  puts "x is greater than y!"
else
  puts "x equals y!"
end
```

### unless 

- Sometimes you want to use control flow to check if something is false, rather than if it’s true. 
- You could reverse your if/else, but Ruby will do you one better: it will let you use an unless statement.
  - Unless sẽ ngược với if, thay vì check dk đúng thì unless sẽ check dk sai thì thực thi câu lệnh trong khối đó.

```
unless hungry
  # Write some sweet programs
else
  # Have some noms
end
```

### Equal or Not?

- In Ruby, we assign values to variables using =, the assignment operator.

- But if we’ve already used = for assignment, how do we check to see if two things are equal?

- Well, we use ==, which is a comparator (also called a relational operator). 

- == means “is equal to.” When you type

### Less Than or Greater Than

- Less than: <
- Less than or equal to: <=
- Greater than: >
- Greater than or equal to: >=

### And

- Comparators aren’t the only operators available to you in Ruby. 
- You can also use logical or boolean operators. Ruby has three: and (&&), or (||), and not (!). Boolean operators result in boolean values: true or false.
```ruby
true && true # => true
true && false # => false
false && true # => false
false && false # => false
```

### Or

- Ruby also has the or operator (||). 
- Ruby’s || is called an inclusive or because it evaluates to true when one or the other or both expressions are true. 
- Check it out:
``` ruby
true || true # => true
true || false # => true
false || true # => true
false || false # => false
```

### Not

- Finally, Ruby has the boolean operator not (!). - ! makes true values false, and vice-versa.
```ruby
!true # => false
!false # => true
```

### Combining Boolean Operators

- You can combine boolean operators in your expressions. 
- Combinations like

```ruby
(x && (y || w)) && z
```

# What You'll Be Building

- Now that we can direct our program using if / else statements, we can produce different results based on different user input.

- In this project, we’ll combine control flow with a few new Ruby string methods to Daffy Duckify a user’s string, replacing each "s" with "th".

```ruby
print "Thtring, pleathe!: "
user_input = gets.chomp
user_input.downcase!

if user_input.include? "s"
  user_input.gsub!(/s/, "th")
else
  puts "Nothing to do here!"
end
  
puts "Your string is: #{user_input}"
```
